# Use the official Golang image to create a build artifact.
# This is based on Debian and sets the GOPATH to /go.
FROM docker.io/library/golang:alpine as builder

# Copy local code to the container image.
WORKDIR /app
COPY app .

# Build the command inside the container.
RUN go build -o server .

# Use a Docker multi-stage build to create a lean production image.
FROM gcr.io/distroless/base-debian10
COPY --from=builder /app/server /server

# Run the web service on container startup.
CMD ["/server"]
