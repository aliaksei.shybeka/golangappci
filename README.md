 Golang Web App with GitLab CI

This project is a Golang web application set up with GitLab CI for continuous integration and deployment. It is a REST API server for checking if a phrase is a palindrome. The application, packaged into a Docker image and stored in GitLab Container Registry, is designed to handle liveness and readiness probes and is accessible over HTTP from the internet via a Kubernetes ingress controller. There are two server pods to process incoming requests in a balanced manner.

## Project Layout

The project is organized into the following folders:

* **app directory**: Contains the code for the Golang web application.
* **ci**: Contains the configuration for GitLab CI and the scripts to build, test, and deploy the application.
* **Chart.yaml**: Describes the Kubernetes helm chart.
* **Dockerfile**: Contains the instructions for building the Docker image.
* **README.md**: Contains the project's description, documentation, and usage instructions.
* **gitlab-ci.yml**: Controls what the build, test, and deploy pipelines do.
* **helm directory**: Contains the Helm charts for deploying the application to Kubernetes.


## Getting Started

To get started with this project, you will need to:

1. Install and configure GitLab Runner. The Runner was created on an Ubuntu 22.04 virtual machine with Podman, Kubectl, and Helm installed. It was then connected to GitLab following the instructions on the GitLab's website. Please note that it is a shell runner (the CI file is configured for this type of runner).
2. Store the `kubeconfig` file for connection to k8s cluster on your GitLab Runner. Considering security implications and if your Runner is situated in a secure environment, you can decide to store your `kubeconfig` directly on the Runner machine. This file is usually located in the `.kube/config` directory of your home folder. Ensure that the `KUBECONFIG` environment variable on the Runner points to this file for subsequent use with `kubectl` and `helm`.

    Care should be taken with this approach as if your Runner is compromised, attackers would gain access to your `kubeconfig`. As previously noted, it is recommended to use a `kubeconfig` associated with a service account with minimal necessary permissions to limit potential damage.

    It's also recommended to narrow down access to your Runner, keep it up-to-date, and avoid using it for other tasks. Best practices for security should be adhered to.
3. Install an Nginx-based Ingress Controller in your Kubernetes cluster using a Helm chart. This was done from the runner as follows:
    ```bash
    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm repo update

    # Now install the Nginx Ingress Controller using the Helm chart
    helm install ingress-nginx ingress-nginx/ingress-nginx
    ```

4. Clone the repository into your local directory and start working with the code.

## How It Works

This project uses GitLab CI/CD to automate the build, test, and deploy process.

The GitLab CI/CD configuration is defined in the `.gitlab-ci.yml` file, which outlines the steps the CI/CD pipeline will take when a change is pushed to the repository:

1. **Set Docker Image Tag**: In the `before_script`, it checks whether there's a tag attached to this commit. If there is, the Docker image tag will be set to the commit tag. Otherwise, it will be set as `latest`.

2. **Build Stage (`build`)**: In the `build` stage, a Docker image is built using the existing codebase. This is triggered exclusively by any change to the `main` branch.

3. **Push Stage (`push`)**: In the `push` stage, the Docker image gets pushed to GitLab's Container Registry. This step requires the Docker image tag mentioned above.

4. **Deploy Stage (`deploy`)**: In the `deploy` stage, the application is deployed to Kubernetes using Helm. It applies the Docker image built and pushed in previous stages for updates in the Helm chart or for installation purposes.

This way, GitLab's CI/CD pipeline automatically handles building, pushing, and deploying the project to Kubernetes upon every `main` branch update. This pipeline enhances the speed, consistency, and traceability of deployment processes.