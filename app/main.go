package main

import (
	"encoding/json"
	"net/http"
	"strings"
)

type Payload struct {
	String string `json:"string"`
}

type Response struct {
	String string `json:"string"`
	Result string `json:"isPalindrome"`
}

func main() {
	http.HandleFunc("/palindrome", handleRequest)
	http.HandleFunc("/healthz", handleHealthCheck)
	http.HandleFunc("/ready", handleReadinessCheck)
	http.ListenAndServe(":8080", nil)
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var payload Payload
	_ = decoder.Decode(&payload)
	palindromeCheck := isPalindrome(payload.String)

	var response Response
	response.String = payload.String
	if palindromeCheck {
		response.Result = "is a palindrome"
	} else {
		response.Result = "is NOT a palindrome"
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleHealthCheck(w http.ResponseWriter, _ *http.Request) {
	// Health check function dummy
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}

func handleReadinessCheck(w http.ResponseWriter, _ *http.Request) {
	// Readiness check function dummy
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}

func isPalindrome(s string) bool {
	right := len(s) - 1
	left := 0
	s = strings.ToLower(s)
	for left < right {
		if s[left] != s[right] {
			return false
		}
		left++
		right--
	}
	return true
}